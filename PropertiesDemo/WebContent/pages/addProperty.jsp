<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="UTF-8">
<title>Registration</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>
	<div class="fixed-head">
		<div class="head-content">Add Property</div>
	</div>
	<div class="content">
		<div class="main clearfix">
			<div class="main-content">
				<div class="col-sm-6 form-styles">
					<form method="post" action="registrationProcess" id="propertyForm"
						enctype="multipart/form-data" name="stuform">
						<div class="form-group">
							<input type="text" name="key" placeholder="Key"
								required="required" class="form-control" id="key" />
						</div>
						<div class="form-group">
							<input type="text" name="value" placeholder="Query"
								required="required" class="form-control" id="email" />
						</div>
						<p class="button_styles" align="center">
							<button type="button" class="btn btn--right" onclick="register()">Register</button>
						</p>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
  function register() {
	 $.ajax({
		 url : 'saveProperties',
			method : 'POST',
			dataType : "json",
			processData : false,
			data : $('#propertyForm').serialize(),
			success : function(response) {
				console.log("sd");
			}
			    	
	    });
	}
</script>
</html>
