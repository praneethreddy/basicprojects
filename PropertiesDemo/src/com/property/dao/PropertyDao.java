package com.property.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.property.pojo.PropertyPojo;

@Repository(value = "propertyDao")
public class PropertyDao {

	@Autowired
	JdbcTemplate JdbcTemplate;

	public int saveProperty(PropertyPojo pojo) {
		String query = "INSERT INTO properties(`key`,`value`) VALUES(?,?)";
		return JdbcTemplate.update(query, new Object[] {pojo.getKey(),pojo.getValue()});
	}
}
