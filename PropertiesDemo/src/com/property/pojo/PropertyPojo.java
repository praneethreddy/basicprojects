package com.property.pojo;

public class PropertyPojo {
	
	private Integer id;
	private String key;
	private String value;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public PropertyPojo(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}
	public PropertyPojo(Integer id, String key, String value) {
		super();
		this.id = id;
		this.key = key;
		this.value = value;
	}
	

}
