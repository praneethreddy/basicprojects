package com.properties.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.property.dao.PropertyDao;
import com.property.model.PropertyModel;
import com.property.pojo.PropertyPojo;

@Controller
public class PropertyController {

	@Autowired
	PropertyDao propertyDao;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView showHomePage(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("homePage");
		return mav;
	}
	
	@RequestMapping(value = "/addProperty", method = RequestMethod.GET)
	public ModelAndView addProperty(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("first add property");
		ModelAndView mav = new ModelAndView("addProperty");
		return mav;
	}
	
	
	@RequestMapping(value = "/addProperties", method = RequestMethod.GET)
	public ModelAndView addProperties(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("first add property");
		ModelAndView mav = new ModelAndView("addProperty");
		return mav;
	}
	@ResponseBody
	@RequestMapping(value = "/saveProperties", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> saveProperties(HttpServletRequest req) {
		Map<String, Object> result = new HashMap<String, Object>();
		System.out.println("saveProperties");
		PropertyPojo pojo = new PropertyPojo(req.getParameter("key"), req.getParameter("value"));
		if (propertyDao.saveProperty(pojo) == 0) {
			result.put("message", "property not added");
		} else {
			PropertyModel.saveProperty(req.getParameter("key"),req.getParameter("value"));
			result.put("message", "property added successfully.");
		}
		return new ResponseEntity<Object>(result, HttpStatus.OK);
	}
}
